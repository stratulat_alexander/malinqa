# Malinqa - Session Recorder & Player
![snapbuilder.png](https://bitbucket.org/repo/qkpEpn/images/2925158358-snapbuilder.png)

In development.

  - Record everything your users see and all their actions & interactions.
  - Re-play the recorded sessions.
  - Magic.

### Version
0.01

### Dependencies

* jQuery

### Basic installation example

```html
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="Malinqa.js"></script>
    <script type="text/javascript">
        $(function() {
            /* ... your code ... */
        });
    </script>
```

### Basic Malinqa Instantiation

```js 
    $(function() {
        var Recorder = new Malinqa();
    });
```