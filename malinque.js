/**
 * Malinque - Session Recorder & Player.
 * In development.
 * @author Stratulat Alexander
 */
(function() {

    /**
     * Main parent object.
     * @type {Object}
     */
    var self = new Object();

    /**
     * Malinque Constructor.
     */
    this.Malinque = function() {

        /**
         * Coordinates array.
         * @type {Array}
         */
        self.coords = [];

        /**
         * Default options.
         * @type {Object}
         */
        var defaults = {
            mouseObject: '.mouseObject',
            clickObject: '.clickObject',
            createURL: '/create-session',
            updateURL: '/update-session',
            maxElements: 100000,
            autoStart: true,
        }

        /**
         * Here we replace the empty options with the default ones defined above.
         */
        if (arguments[0] && typeof arguments[0] === "object") {
            self.options = extendDefaults(defaults, arguments[0]);
        } else {
            self.options = defaults;
        }

        /**
         * These can be overridden in CSS with !important. 
         * The class/id of the element is provided manually by user or `.mouseObject` by default.
         * @type {Object}
         */
        self.options.mouseStyle = {
            'position': 'absolute',
            'border-radius': '20px',
            'background': 'red',
            'width': '10px',
            'height': '10px',
            'opacity': 0.5,
            'z-index': 9999,
        }

        /**
         * These can be overridden in CSS with !important.
         * The class/id of the element is provided manually by user or `.clickObject` by default.
         * @type {Object}
         */
        self.options.clickStyle = {
            'position': 'absolute',
            'border-radius': '20px',
            'background': 'blue',
            'width': '10px',
            'height': '10px',
            'opacity': 0.4,
            'z-index': 9999,
        }

        /**
         * Here we check if the selector is for class(.) or for id(#).
         * @type {String}
         */
        self.options.clickObjectType = (self.options.clickObject[0] == '.' ? 'class' : 'id');


        /**
         * Here we check if the selector is for class(.) or for id(#).
         * @type {String}
         */
        self.options.mouseObjectType = (self.options.mouseObject[0] == '.' ? 'class' : 'id');

        /**
         * Let's auto-start the recording on plugin load if autoStart is true.
         */
        if (self.options.autoStart) {
            this.record();
        }

        /**
         * Method chaining.
         */
        return this;

    }

    /**
     * Utility method to extend defaults with user options.
     * @param  {Object} source     Default options.
     * @param  {Object} properties Options provided on instantiation.
     * @return {Object}            Updated options.
     */
    function extendDefaults(source, properties) {
        var property;
        for (property in properties) {
            if (properties.hasOwnProperty(property)) {
                source[property] = properties[property];
            }
        }
        return source;
    }

    /**
     * Click simulation.
     * @param  {DOMElement} elem    The target element to be clicked.
     * @return {Void}
     */
    function click(elem) {
        if (document.createEvent && $(elem)[0].length) {
            var e = document.createEvent("MouseEvents");
            e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            elem[0].dispatchEvent(e);
        } else if (elem.fireEvent) {
            elem[0].fireEvent("onmousedown");
            elem[0].trigger('change');
        }
    }

    /**
     * Recording/tracking process.
     * @return {Object} Method chaining.
     */
    Malinque.prototype.record = function() {

        /**
         * Mouse movement listener.
         */
        $(document).mousemove(function(e) {
            self.coords.push({
                x: e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft,
                y: e.clientY + document.body.scrollTop + document.documentElement.scrollTop,
                clicked: false
            });
        });

        /**
         * Mouse click listener.
         */
        $(document).click(function(e) {
            self.coords.push({
                x: e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft,
                y: e.clientY + document.body.scrollTop + document.documentElement.scrollTop,
                clicked: true
            });
        });

        /**
         * Method chaining.
         */
        return this;
    }

    /**
     * Record player.
     * @return {this} Method chaining.
     */
    Malinque.prototype.play = function() {

        /**
         * Avoid playing a null record.
         */
        if (!self.coords.length) {
            console.error('Please start the recorder. You can call `record()` manually or set `autoStart : true` in the constructor.')
            return false;
        }

        var click_object_selector = self.options.clickObject.substring(1);
        var mouse_object_selector = self.options.mouseObject.substring(1);

        var $click_object = $("*[" + self.options.clickObjectType + "*=" + click_object_selector + "]");
        var $mouse_object = $("*[" + self.options.mouseObjectType + "*=" + mouse_object_selector + "]");

        /**
         * Creating the mouse simulator object for the first time.
         */
        if (!$mouse_object.length) {
            $mouse_object = $('<div/>').attr(self.options.mouseObjectType, mouse_object_selector).clone().appendTo('body').css($.extend(self.options.mouseStyle, {
                top: self.coords[0].y,
                left: self.coords[0].x,
            }));
        }

        /**
         * Here we delete all the click markers if we re-play the record.
         */
        $click_object.remove();

        /**
         * Unbind all the listeners during play.
         */
        $(document).off("mousemove").off("click");

        /**
         * The core animation.
         */
        $.each(self.coords, function(index, coord) {

            $mouse_object.animate({
                top: coord.y,
                left: coord.x
            }, 20, 'linear', function() {

                /**
                 * Let's check if the user was clicking in the particular moment.
                 */
                if (coord.clicked) {

                    /**
                     * And of course, let's draw the marker on the click location.
                     */
                    $('<div/>').attr(self.options.clickObjectType, click_object_selector).clone().appendTo('body').css($.extend(self.options.clickStyle, {
                        top: coord.y,
                        left: coord.x
                    }));

                    /**
                     * Let's simulate the click for inputs, selects, buttons, etc.
                     */
                    click(document.elementFromPoint(coord.x, coord.y));

                }

            });

        });

        /**
         * Method chaining.
         */
        return this;
    }

}());